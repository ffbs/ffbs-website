ffbs-website
============

Die Website des Freifunk Braunschweig

Alle Seiten werden statisch generiert. Die Templates werden mit Mustache gebaut, das entsprechende Tool zum
Generieren der HTML Dateien findet sich hier: https://github.com/tests-always-included/mo
Um das Generieren einfach zu machen ist ``mo`` aber auch in diesem Repo abgelegt.

Falls eine andere als die lokale ``mo``-Version verwendet werden soll muss der Pfad in ``generate.sh`` entsprechend
angepasst werden.

Um die Seite zu generieren einfach ``./generate.sh`` im root des Projektes ausführen.

Zum Ansehen wird ein lokaler HTTP-Server benötigt. z.B. Python bringt sowas mit: ``python3 -m http.server``.

Folgende Konfiguration liegt im nginx vor, diese Seiten werden von einem anderen Server bearbeitet:

	location ~ ^/(api.json|nodes.json|nodes_compat.json|nodes.txt|nodecount|newkey|clientcount|deny_key|approve_key|contact)$


