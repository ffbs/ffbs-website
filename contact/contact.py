#!/usr/bin/env python3
# coding=utf-8

from aiohttp import web
import asyncio
import re
import smtplib
import uuid
import datetime
from email.mime.text import MIMEText
from subprocess import Popen, PIPE

import logging
logging.basicConfig(level=logging.INFO)

CONTACT_EMAIL_TO = 'kontakt@freifunk-bs.de'

EMAIL_REGEX = r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)"

template = """
<html>
<head>
<meta charset="utf-8">
</head>
<body>
%s<br>
<a href="/kontakt.html">zurück</a>
</body>
</html>
"""

async def handle(req):
    logging.debug('Got a new request:')
    form = await req.post()
    logging.debug(f'Form data: {form}')
    try:
        email = form['email']
        if not re.match(EMAIL_REGEX, email):
            raise ValueError()
        message = form['message']
        
        captcha = form['captcha']
        captcha = captcha.replace("'", "")
        captcha = captcha.replace('"', "")
        captcha = captcha.strip()

        today = str(datetime.datetime.now().day)
        if today == captcha:
            logging.info('CAPTCHA successfull')
            msg = MIMEText(message, 'plain', 'utf-8')
            msg['From'] = "kontakt@freifunk-bs.de"
            msg['To'] = CONTACT_EMAIL_TO
            msg['Subject'] = 'Kontaktanfrage von %s' % email
            msg['Reply-To'] = ','.join([email,CONTACT_EMAIL_TO])
            ruuid = str(uuid.uuid4()) #get a random uuid
            msg['Message-ID'] = '<'+ruuid+'@freifunk-bs.de>'
            p = Popen(["/usr/sbin/sendmail", "-t", "-oi", "-FKontaktformular"], stdin=PIPE)
            p.communicate(msg.as_string().encode())
            logging.info('Mail sent')
            return web.Response(body = template % "Ihre Nachricht wurde entgegengenommen", content_type='text/html')
        else:
            logging.info('CAPTCHA NOT successfull')
            return web.Response(body = template % "Der Spamschutz wurde nicht erfolgreich ausgefüllt. Bitte versuchen Sie es erneut.", content_type='text/html')
    except Exception as e:
        logging.exception('Request failed! Exception:')
        return web.Response(body = template % "Die Anfrage ist ungültig", content_type='text/html')

app = web.Application()
app.add_routes([web.post('/contact', handle)])

if __name__ == '__main__':
    web.run_app(app, host='0.0.0.0', port=7392)

# vim: expandtab:shiftwidth=4:softtabstop=4
