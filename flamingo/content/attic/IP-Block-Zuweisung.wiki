disabled: true
title: IP Block Zuweisung

Hier ist eine Auflistung der derzeit verwendeten ip Netze und für was und für wen


{| border="1" cellpadding="2" cellspacing="0" style="text-align:center;"
|-
! Subnetz !! use !! Verwaltet !! Ansprechpartner
|-
| 10.38.1.0/24 || Openvpn einwahl auf GW1 || Openvpn || oni
|-
| 10.38.10.0 - 10.38.19.254 || Client Net || dhcp GW 1 ||
|-
| 10.38.20.0 - 10.38.29.254 || Client Net || dhcp GW 2 ||
|-
| 10.38.30.0 - 10.38.39.254 || Client Net || dhcp GW 3 ||
|-
| 10.38.40.0 - 10.38.49.254 || Client Net || dhcp GW 4 ||
|-
| 10.38.128.0/23 || R&D || static || lichtfeind
|-
| 10.38.200.0/27 || Richtfunkstrecken || static || Sebastian
|-
| 10.38.200.32/27 || Test || static || Alexander 
|-
| 10.38.200.33 || Unterrichtsmaterial || static || Alexander
|-
| 10.38.222.1/27 || craftbeerpi3 || static || larsan
|-
|}
