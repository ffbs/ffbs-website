sort: 50
title: Router einrichten


Router einrichten
=================

Diese Anleitung ist für unterstützte Geräte von TP-Link und Netgear.
Bei Geräten von Ubiquity oder AVM Fritz!Box funktioniert das Installieren
der Firmware anders als hier beschrieben.

Router anschließen
------------------

1. Lade das passende Erstinstallations-Firmware-Image für Deinen Router von `unserer Seite <https://firmware.freifunk-bs.de/#stable>`__ herunter.
2. Verbinde Deinen Computer mit einem der gelben LAN-Anschlüsse des Routers.
3. Dein Computer erhält nun automatisch via DHCP eine IP-Adresse.

Installation
------------

Mit Hersteller-Firmware
_______________________

Ist auf Deinem Router noch die Firmware des Herstellers installiert, sind die genauen Schritte abhängig vom Hersteller Deines Gerätes.

#. Öffne das Webinterface Deines Routers in deinem Browser.
#. Folge den Anweisungen im Handbuch Deines Routers um die Firmware zu aktualisieren.
#. Nutze nun das von der zuvor heruntergeladene Firmware-Image.
#. Warte bis der Router wieder gestartet ist.

Mit Freifunk-Firmware
_____________________

Ist auf Deinem Router bereits eine Freifunk-Firmware installiert, ist diese Anleitung für Dich relevant.

#. Starte den Router im Konfigurationsmodus neu. \
   Wie dies funktioniert ist in unserer `FAQ </faq/faq.html>`__ erklärt.
#. Gehe in den *Expertenmodus*.
#. Gehe auf den Reiter *Firmware aktualisieren*.
#. Wähle die Firmware-Datei aus und lade diese hoch.
#. Bei Bedarf können die Einstellungen des Gerätes zurückgesetzt oder beibehalten werden.
#. Werden die Einstellungen beibehalten, startet das Gerät anschließend in den Betriebsmodus neu.
#. Werden die Einstellungen nicht beibehalten, startet das Gerät in den Konfigurationsmodus neu.


Konfigurationsmodus
-------------------

Dein Router sollte nun in den Konfigurationsmodus starten. Du erhälst erneut eine IP-Adresse per DHCP, dein Freifunk-Router ist nun unter http://192.168.1.1 in Deinem Browser erreichbar.


Den Router konfigurieren
------------------------

#. Wähle einen Namen oder verwende den bereits eingetragenen Namen.
#. Aktiviere das Mesh-VPN, um eine Verbindung mit dem Freifunk-Netz über das Internet herzustellen.
#. Füge GPS-Koordinaten des Router-Standorts ein. Diese kannst du auf unterschiedliche Weisen erhalten:

   - Ist Dein Rechner gleichzeitig mit dem Router und dem Internet verbunden kannst
     Du die Koordinaten direkt auf der Seite des Konfigurationsmodus auswählen.

   - Alternativ kannst Du die Koordinaten bei einem Kartendienst wie
     `Openstreetmap <https://openstreetmap.org>`__ ermitteln.

#. Du hast die Option, eine Kontaktmöglichkeit anzugeben. Diese ist entweder nur für Administratoren, \
   oder öffentlich für alle sichtbar, je nachdem wie du möchtest.
#. Nach einem Klick auf Speichern und Neustarten wird die Konfiguration beendet. Seit einer Änderung \
   Ende 2021 wird an dieser Stelle kein Key mehr angezeigt, um ihn auf unserer Webseite einzutragen.
   Dies geschieht nun automatisch.

Danach startet der Router neu, sendet das WLAN 'Freifunk' aus und versucht sich per Mesh
mit anderen Nachbarn, sowie sich über seinen Uplink direkt selbst mit unserem Netz zu verbinden.

Der Router ist jetzt im Normalbetrieb und nicht mehr unter http://192.168.1.1 erreichbar.
Für spätere Änderungen kann der Router wieder in den Konfigurationsmodus versetzt werden.


Den Router aufstellen
---------------------

Als Standort empfehlen sich Fenster, Balkone oder Dächer.
Je näher er der Außenwelt ist, desto wahrscheinlicher ist es, dass er für Passanten erreichbar
ist und sich mit anderen Routern verbindet um eine Meshverbindung zu bilden.


Verbinde den Router mit dem Netzwerk
------------------------------------

Wenn Du Deinen Internetzugang zur Verfügung stellen möchtest, verbinde jetzt den WAN-Anschluss
Deines Freifunk-Router mit deinem vorhandenen Internetrouter. Möchtest oder kannst Du das nicht,
bist Du auf andere Freifunker in deiner Nähe angewiesen um eine Verbindung zum Freifunk-Netzwerk
und ins Internet herzustellen.


Fertig
------

Super, Du hast es geschafft.
Dein Freifunk-Knoten funktioniert jetzt und sollte, wenn Du GPS Koordinaten angegeben hast,
in Kürze auf der `Karte <https://freifunk-bs.de/map/#!v:m>`__ erscheinen.

.. raw:: html

   <i>

Diese Anleitung wurde erstellt mit Inhalten unter
`CC-BY-SA 4.0 <https://creativecommons.org/licenses/by-sa/4.0/>`__
des
`Freifunk Darmstadt <https://darmstadt.freifunk.net/mitmachen/router-einrichten/>`__.

.. raw:: html

   </i>




