sort: 300
title: Flashen der Fritz!Box 7412


Informationen zum Flashen einer Fritz!Box 7412
==============================================

Vorbereitung
------------

Folgende Files werden benötigt (Versionen zum Zeitpunkt des schreibens 
des Artikel):

* `eva_ramboot.py <https://git.openwrt.org/?p=openwrt/openwrt.git;a=blob_plain;f=scripts/flashing/eva_ramboot.py>`__
* `openwrt-initramfs-image für Fritz!Box 7412 <https://downloads.openwrt.org/releases/19.07.7/targets/lantiq/xrx200/openwrt-19.07.7-lantiq-xrx200-avm_fritz7412-initramfs-kernel.bin>`__
* `freifunk sysupgrade image <https://www.freifunk-bs.de/firmware/?q=AVM%E2%81%A3%20FRITZ!Box%C2%A07412%E2%81%A3%20alle%E2%81%A3%20Upgrade>`__

**Nach** dem Download der Dateien wird das Netzwerk vorbereitet:

* WLAN ausschalten (gemeint sind Verbindungen zu Netzwerken die IPs in 
  den Netzwerken 192.168.178.0/14 und 192.168.1.0/24 haben)
* Ethernet-interface auf 192.168.178.10/24 und 192.168.1.10/24 
  konfigurieren (/24 meint Netmask 255.255.255.0)
* Ethernet Kabel mit der LAN-Steckdose der Fritz!Box verbinden

3 Terminals öffnen:

+------------+----------------------------+
| Terminal 1 | zum ausführen von Skripten |
+------------+----------------------------+
| Terminal 2 | *ping 192.168.178.1*       |
+------------+----------------------------+
| Terminal 3 | *ping 192.168.1.1*         |
+------------+----------------------------+

Erster Flash: OpenWRT-Firmware
------------------------------

Zum ersten Flash wie folgt vorgehen:

1. Die Fritz!Box einschalten
2. nach einigen Sekunden -- wenn in *Terminal 2* der Ping erfolgreich 
   ist -- folgenden Befehl im *Terminal 1* ausführen

.. code-block:: none

  ./scripts_flashing_eva_ramboot.py 192.168.178.1 openwrt-19.07.7-lantiq-xrx200-avm_fritz7412-initramfs-kernel.bin

Es dauert ca. 1 Minute, bis die Fritz!Box durchgestartet ist und im 
*Terminal 3* der Ping erfolgreich ist. (Durch den Reboot pingt es noch 
kurz in Terminal 2, nicht erschrecken.)

Hat das nicht geklappt, dann den Vorgang noch einmal durchführen: 
Ausschalten -> Einschalten -> script ausführen, wie oben beschrieben

Test ob erfolgreich
-------------------

Im *Terminal 1* mit der Fritz!Box verbinden

.. code-block:: none

   ssh root@192.168.1.1

Der Login sollte ohne Passwort funktionieren, anschließend wieder 
ausloggen (*<STRG>-<D>* oder *exit*).

Zweiter Flash: Freifunk-Firmware installieren
---------------------------------------------

Die Freifunk-Firmware auf den Router installieren und dazu in *Terminal 
1* folgendes eingeben:

.. code-block:: none

   scp gluon-ffbs-next-v2020.2.2.2-ffbs-next-202103281243-parker_beta-avm-fritz-box-7412-sysupgrade.bin 192.168.1.1:/tmp

   ssh root@192.168.1.1

   sysinstall /tmp/gluon-ffbs-next-v2020.2.2.2-ffbs-next-202103281243-parker_beta-avm-fritz-box-7412-sysupgrade.bin

Freifunk konfigurieren
----------------------

Nun kann wie gehabt über http://192.168.1.1 die Konfiguration erfolgen.

Troubleshooting: Wenn es einfach nicht klappt
---------------------------------------------

Es kann vorkommen, dass nach dem reboot wieder die Orginal-Firmware zu 
sehen ist. Dann kann folgendes helfen:

* Fritz!Box einschalten
* in *Terminal 1* mit *ftp 192.168.1.1* mit der Fritz!Box verbinden, 
  sobald in *Terminal 2* der Ping funktioniert
* Benutzer: adam2
* Passwort: adam2
* Eingabe von *quote SETENV linux_fs_start 0*
* Fritz!Box neu starten und mit dem Flashen (s.o.) beginnen

Dadurch wird die richtige Boot-Partition ausgewählt.

Tips und Tricks
---------------

Auto-Vervollständigung über die TAB-Taste
.........................................

Wer Linux noch nicht genutzt hat, kennt es vielleicht nicht. Aber es 
macht das Leben unwahrscheinlich einfach, die TAB-Taste in der Shell.

Beispiel: der *sysinstall*-Befehl. Es ist ziemlich mühsam, das ganze 
*gluon-ffbs-next-v2020.2.2.2-ffbs-next-202103281243-parker_beta-avm-fritz-box-7412-sysupgrade.bin* 
einzutippen. Das geht dann viel einfacher über:

*sysinstall /tmp/gl* und nun einmal die TAB-Taste drücken. Da es keine 
weitere Datei in dem Verzeichnis */tmp/* gibt, wird der Rest einfach 
ersetzt.

Ausschalten der nervigen key-Fehler
...................................

Wenn man öfters Router flashed, dann kommt bei der Verbindung via SSH 
ein *Host-Key-Exchange-Error* und man muss den Host-Identifier manuell 
löschen. Das nervt mit der Zeit.

Hier hilft das Anlegen einer *~/.ssh/config* Datei mit folgendem Inhalt:

.. code-block:: none

   Host neu
      User root
      Hostname 192.168.1.1
      IdentityFile .ssh/id_rsa_ff_router
      StrictHostKeyChecking no
      LogLevel ERROR
      UserKnownHostsFile /dev/null

Daduch vereinfachen sich die Befehle von oben ein wenig. aus 
*192.168.1.1* wird *neu*. Ein Paar Beispiele:

.. code-block:: none

   ## vorher
   ssh root@192.168.1.1
   
   ## nachher
   ssh neu
   
   ## vorher
   scp <FIRMWAREFILE> 192.168.1.1:/tmp
   
   ## nachher
   scp <FIRMWAREFILE> neu:/tmp

