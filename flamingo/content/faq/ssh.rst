sort: 300
title: SSH-Zugang auf den eigenen Router


SSH-Zugang auf dem eigenen Router
=================================

Es ist möglich sich aus dem Freifunk-Netz per ssh auf dem eigenen Router
einzuloggen.

Vorbereitung
------------

Um später per SSH zugreifen zu können muss auf dem Router ein SSH-Key
oder ein Passwort eingerichtet worden sein.
Wir empfehlen den Zugriff per SSH-Key und nicht per Passwort abzusichern,
da SSH-Keys ein höheres Maß an Sicherheit ermöglichen.

SSH-Keys und Passwörter werden im Konfigurationsmodus unter *Erweitert*
hinterlegt.

Vorbereitung unter Windows
--------------------------

Unter Windows ist hierzu bspw. die Open Source SW
`Putty <http://www.putty.org/>`__  notwendig.

Vorbereitung unter Linux
------------------------

Ein Linux-basierendes Betriebssystem, wie z.B. Ubuntu oder Debian,
bringen alle Voraussetzungen für den SSH-Zugriff bereits mit.

Ermitteln der IP des Routers
----------------------------

Bei direkter Verbindung
***********************

Bist du per WLAN oder Kabel direkt mit deinem Zielrouter verbunden,
ist dies trivial:
Der Router, mit dem du direkt verbunden bist ist immer unter den
folgenden IPs erreichbar:

.. code-block:: none

   172.16.127.1
   2001:bf7:382::1

IP über die Karte herausfinden
******************************

Auf unserer Karte kannst du die IP eines jeden Routers ermitteln.
Gehe hierzu auf `die Karte  <https://map.freifunk-bs.de/>`__
und suche nach deinem Router.
In den Eigenschaften deines Routers findest du unter *IP Addressen*
zwei Eintäge, wie z.B.:

.. code-block:: none

    2001:bf7:380::32b5:c2ff:fec2:772c
    fe80::32b5:c2ff:fec2:772c

Benutze für den Zugriff die Adresse, die mit *2001:* beginnt.

Der harte Weg: Multicast-Ping
*****************************

Auf einem Linux-System kannst du ausserdem die Adressen aller
Geräte via IPv6 Multicast-Ping ermitteln.

Achtung: Diese Variante ist komplex und fehleranfällig!
(Dafür funktioniert sie auch, wenn man keinen Webbrowser verwenden kann :w

Die IP-Adresse deines Routers findest du heraus, indem du z. B. im Freifunk-Netz an alle Geräte via multicast pingst:

.. code-block:: none

   ping6 -c2 -i4 ff02::1%wlan0        #wenn du per wlan verbunden bist
   ping6 -c2 -i4 ff02::1%eth0         #wenn du per ethernet verbunden bist

Dein Netzwerkinterface kann einen anderen Namen als *wlan0*,
oder *eth0* haben, den kannst du mit *iwconfig* herausfinden.

Du erhälst dann eine (schnell wachsende) Liste, das Gerät,
was am schnellsten Antwortet (nicht unbedingt der erste Eintrag)
– üblicherweise im niedrigen, einstelligen ms-Bereich – ist der
Router, mit dem du gerade verbunden bist.

Die Link-Local-Adresse wird von der MAC des APs abgeleitet:
*fe80::26a4:3cff:fe9e:2e99* gehoert also zum AP mit der MAC 
*24:a4:3c:9e:2e:99*.


Zugriff via SSH unter Linux
---------------------------

Ausgestattet mit der IP deines Routers kannst du nun per SSH auf
deinen Router zugreifen.
Öffne ein Terminal und gebe folgendes Kommando ein.
Ersetze dabei <IP> durch die Adresse, die du oben ermittelt hast.

.. code-block:: none

   ssh root@<IP>

Also z.B.:

.. code-block:: none

   ssh root@2001:bf7:380::32b5:c2ff:fec2:772c

Zugriff via SSH unter Windows
-----------------------------

Starte hierzu die *putty.exe* und gebe dort die IP-Adresse, sowie
den Benutzernamen *root* an.



