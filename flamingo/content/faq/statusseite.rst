sort: 400
title: Knoten Statusseite


Knoten Statusseite
===================

Ist dein Freifunkknoten einmal eingerichtet, gibt es nichts weiter zu
konfigurieren oder zu tun.
Ein Webinterface, wie du es vielleicht von deinem Heimrouter kennst, gibt es
bei einem Knoten nicht.
Stattdessen gibt es eine Statusseite mit vielen interessanten Informationen.

Die Statusseite ist im Freifunk erreichbar und zeigt immer den Status des
Router an, mit dem man gerade verbunden ist.
Die Stautsseite kann unter `http://node.ffbs <http://node.ffbs>`__ erreicht
werden.


Optimierung von Mesh-Verbindungen
---------------------------------

Im rechten Teil der Seite werden Empfangsfeldstärke und Linkqualität
der Mesh-Verbindungen zu anderen Routern angezeigt.

Bei der Empfangsfeldstärke gilt: je näher die Zahl an 0 dB, je besser.
Der Wert sollte immer größer  als -80 dB sein.
Soll die Verbindung produktiv genutzt werden ist ein Wert von über
-70 dB anzustreben.

Je höher der Wert ist, je weniger *Airtime* wird der Link benötigen
und je weniger Pakete werden verloren gehen.

Die Diagramme können genutzt werden, um die Position der beiden beteiligten
Router zu optimieren.

Statusseite erreichen, wenn Freifunk nicht richtig funktioniert
---------------------------------------------------------------

Die Statusseite kann über `http://node.ffbs <http://node.ffbs>`__
nur erreicht werden, wenn die Verbindung zum
Freifunk Netz (bzw. den Gateways) funktioniert.

Funktioniert diese Verbindung nicht kann die Statusseite über
`http://[2001:bf7:382::1]/ <http://[2001:bf7:382::1]/>`__
erreicht werden.

Alternativ ist die Statusseite per IPv4 auch unter
`http://172.16.127.1 <http://172.17.127.1>`__ erreichbar.
