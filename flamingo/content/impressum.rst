template: page.html


Impressum
=========

.. role:: raw-html(raw)
    :format: html

Stratum 0 e.V.
:raw-html:`<br />`
Hamburger Straße 273A
:raw-html:`<br />`
38114 Braunschweig
:raw-html:`<br />`
E-Mail: `vorstand@stratum0.org <mailto:vorstand@stratum0.org>`__ (nicht für Anfragen zu Freifunk)
:raw-html:`<br />`
Anfragen zu Freifunk bitte an `kontakt@freifunk-bs.de <mailto:kontakt@freifunk-bs.de>`__ oder über `Kontaktformular </kontakt.html>`__.
:raw-html:`<br />`
**Vertreten durch** den `Vorstand <https://stratum0.org/wiki/Arbeitsgruppe:Gesch%C3%A4ftsf%C3%BChrung>`__.
:raw-html:`<br />`
:raw-html:`<br />`
**Registereintrag:**
:raw-html:`<br />`
Eingetragen im Vereinsregister.
:raw-html:`<br />`
Registergericht: Amtsgericht Braunschweig
:raw-html:`<br />`
Registernummer: 200889
:raw-html:`<br />`

Das Impressum basiert auf einer
`Impressums-Vorlage <http://www.impressum-recht.de/impressum-generator/>`__.

Datenschutzerklärung
--------------------

Datenschutz
***********

Nachfolgend möchten wir Sie über unsere Datenschutzerklärung informieren. Sie finden hier Informationen über die Erhebung und Verwendung persönlicher Daten bei der Nutzung unserer Webseite. Wir beachten dabei das für Deutschland geltende Datenschutzrecht. Sie können diese Erklärung jederzeit auf unserer Webseite abrufen.

Wir weisen ausdrücklich darauf hin, dass die Datenübertragung im Internet (z.B. bei der Kommunikation per E-Mail) Sicherheitslücken aufweisen und nicht lückenlos vor dem Zugriff durch Dritte geschützt werden kann.

Die Verwendung der Kontaktdaten unseres Impressums zur gewerblichen Werbung ist ausdrücklich nicht erwünscht, es sei denn wir hatten zuvor unsere schriftliche Einwilligung erteilt oder es besteht bereits eine Geschäftsbeziehung. Der Anbieter und alle auf dieser Website genannten Personen widersprechen hiermit jeder kommerziellen Verwendung und Weitergabe ihrer Daten.

Personenbezogene Daten
**********************

Sie können unsere Webseite ohne Angabe personenbezogener Daten besuchen. Soweit auf unseren Seiten personenbezogene Daten (wie Name, Anschrift oder E-Mail Adresse) erhoben werden, erfolgt dies, soweit möglich, auf freiwilliger Basis. Diese Daten werden ohne Ihre ausdrückliche Zustimmung nicht an Dritte weitergegeben. Sofern zwischen Ihnen und uns ein Vertragsverhältnis begründet, inhaltlich ausgestaltet oder geändert werden soll oder Sie an uns eine Anfrage stellen, erheben und verwenden wir personenbezogene Daten von Ihnen, soweit dies zu diesen Zwecken erforderlich ist (Bestandsdaten). Wir erheben, verarbeiten und nutzen personenbezogene Daten soweit dies erforderlich ist, um Ihnen die Inanspruchnahme des Webangebots zu ermöglichen (Nutzungsdaten). Sämtliche personenbezogenen Daten werden nur solange gespeichert wie dies für den geannten Zweck (Bearbeitung Ihrer Anfrage oder Abwicklung eines Vertrags) erforderlich ist. Hierbei werden steuer- und handelsrechtliche Aufbewahrungsfristen berücksichtigt. Auf Anordnung der zuständigen Stellen dürfen wir im Einzelfall Auskunft über diese Daten (Bestandsdaten) erteilen, soweit dies für Zwecke der Strafverfolgung, zur Gefahrenabwehr, zur Erfüllung der gesetzlichen Aufgaben der Verfassungsschutzbehörden oder des Militärischen Abschirmdienstes oder zur Durchsetzung der Rechte am geistigen Eigentum erforderlich ist.

Auskunftsrecht
**************

Sie haben das jederzeitige Recht, sich unentgeltlich und unverzüglich über die zu Ihrer Person erhobenen Daten zu erkundigen. Sie haben das jederzeitige Recht, Ihre Zustimmung zur Verwendung Ihrer angegeben persönlichen Daten mit Wirkung für die Zukunft zu widerrufen. Zur Auskunftserteilung wenden Sie sich bitte an den Anbieter unter den Kontaktdaten im Impressum. 
