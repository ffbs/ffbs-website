sort: 200
title: Mailingliste


Mailingliste
============

Neben unseren Treffen und dem Chat via `IRC </irc.html>`__ sind unsere
Mailinglisten ein wichtiger Baustein für unsere Kommunikation.

Community-Mailingliste
----------------------

Dies ist unsere allgemein Mailingliste für Ankündigungen und Diskussionen
rund um Freifunk Braunschweig.
Wenn du informiert bleiben möchtest oder Fragen an die Community stellst ist
das hier der richtige Ort.

Die Mailingliste wird von `freifunk.net <https://freifunk.net>`__
zur Verfügung gestellt.
Du kannst dich unter
`lists.freifunk.net <http://lists.freifunk.net/mailman/listinfo/braunschweig-freifunk.net>`__
für unsere Mailingliste eintragen.

Technische Mailingliste
-----------------------

Neben der Community-Mailingliste betreiben wir eine Admin-Mailingliste für
das Kernteam von Freifunk Braunschweig. Auf dieser Liste gibt es
automatisierte technische Meldungen und Diskussionen über die Infrastruktur
im Hintergrund und auf den Servern.

Die Mitgliedschaft auf dieser Mailingliste steht prinzipiell jedem offen,
der sich an der Entwicklung und Verbreitung von Freifunk beteiligen möchte.
Sprich uns dafür am Besten auf einem der regelmäßigen Treffen an.
