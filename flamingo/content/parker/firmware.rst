template: firmware_parker.html
title: Firmware
sort: 105


Firmware
========

Diese Seite gibt einen Überblick über die Änderungen zwischen unterschiedlichen
Versionen der Freifunk Braunschweig Firmware.

Die aktuellste Firmware kann über den folgenden Link heruntergeladen werden.
Solltest du eine ältere Firmware benötigen kannst du dich über
`Kontakt </kontakt.html>`__ an uns wenden.

:raw-html:`<a class="btn btn-success" style="display: inline-flex; align-items:center;" href="/firmware/" ><i class="fas fa-download" style="font-size:150%; margin-right:0.5em;"></i> Firmware herunterladen</a>`
