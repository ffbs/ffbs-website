branches:
    - name: v2021.1.2.4-ffbs-next
      channel: parker_beta
      date: 2023-02-22
    - name: v2021.1.2.4-ffbs-next
      channel: parker_stable
      date: 2023-03-01

site: "https://gitli.stratum0.org/ffbs/ffbs-site/-/commit/bbcefb21b6c602b4d902be8b4ab46deb7144b2e8"
gluon: "https://gitli.stratum0.org/ffbs/ffbs-gluon/-/commit/8ba1a3b4a76474132361c38683d6680933e4320f"
rtd:   "https://gluon.readthedocs.io/en/v2021.1.2/"


v2021.1.2.4-ffbs-next
=====================

Mit diesem Release wird ein Filter zwischen LAN-Ports und Freifunk-WLAN eingeführt.
Damit soll verhindert werden, dass im Falle eines falschen Anschluss eines Routers
das Heimnetz über Freifunk erreichbar wird.

Normale Kommunkation zwischen Clients im WLAN und Geräten an den LAN-Anschlüssen
wird hierdurch nicht eingeschränkt.

Technische Details zu den Filtern finden sich 
`hier <https://gluon.readthedocs.io/en/latest/package/gluon-ebtables-filter-ra-dhcp.html>`__.

Bekannte Fehler
---------------

*keine*
