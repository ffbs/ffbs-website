branches:
    - name: v2023.2.4.1-ffbs-next
      channel: parker_beta
      date: 2024-02-19

site: "https://gitli.stratum0.org/ffbs/ffbs-site/-/commit/e215550036e52f02b06a9ba9365527fc89764545"
gluon: "https://gitli.stratum0.org/ffbs/ffbs-gluon/-/tree/v2023.2.4.1-ffbs-next?ref_type=tags"
rtd:   "https://gluon.readthedocs.io/en/v2023.2.x/"


v2023.2.4.1-ffbs-next
=====================

Mit dieser Firmware aktualisieren wir auf Gluon von v.2023.2.3 auf v2023.2.4.
Dieses Gluon behebt mehrere kleinere Fehler, die bei uns keine größeren Auswirkungen gehabt haben sollten.
Außerdem werden zwei neue Geräte unterstützt.

Der größte Umbau ist, dass wir unser Gluon Parker so umgebaut haben, dass dies auch von anderen Communities
genutzt werden kann.
Ein paar mehr Infos dazu haben wir im `README <https://github.com/ffbs/gluon-parker/>`__ aufgeschrieben.

Bekannte Fehler
---------------

* Unify AC Geräte haben sporadisch Probleme mit dem Mesh auf 2.4 GHz.
  Das wirkt sich so aus, dass diese Geräte auf der Karte als Offline angezeigt
  werden, obwohl sie vor Ort weiterhin Daten austauschen.
  Die Statusseite ist in diesem Fall nicht mehr erreichbar.

* Geräte mit `mt76`-Wifi, z.B. der D-Link DAP-X1860, zeigen zum Teil nicht sinnvolle
  Airtime-Werte in unserer Visualisierung an.
  Dies scheint ein `bekannter Fehler <https://github.com/openwrt/mt76/issues/916>`__
  in dem Treiber zu sein.
  Die schlechte Visualisierung scheint die Funktion des Wifi aber nicht zu beeinflussen.

Neue Funktionen
---------------

* Testweise werden wir unsere Geräte ab jetzt Donnerstags zwischen 03:00 und
  06:00 Uhr einmal neu Booten.
  Andere Communities haben dieses Vorgehen bereits vor einiger Zeit übernommen.
  Wir wollen nun einmal sehen, wie sich unser Netz damit verhält.
