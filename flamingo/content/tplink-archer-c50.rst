template: page.html


TP-Link Archer C50 V4 flashen
=============================

Der TP-Link Archer C50 V4 lässt sich nur über TFTP erfolgreich flashen. Der Vorgang wird hier im Einzelnen beschrieben.

Dieses Vorgehen benötigt ein Linux oder MacOS und einen funktionierenden TFTP Server.
Ein zusätzlicher Switch hat sich als nützlich erwiesen.

Firmware vorbereiten
--------------------

Zuerst die gewünschte Freifunk Firmware herunterladen und als *ff.bin* abspeichern. Unsere Firmware findet man
`hier <https://firmware.freifunk-bs.de/selector/?q=TP-Link%E2%81%A3%20Archer%C2%A0C50%E2%81%A3%20v4%E2%81%A3%20Upgrade>`__.

Als nächstes eine originale Firmware von TP-Link herunterladen. Im Test hat
`diese Version (180313) <https://static.tp-link.com/2018/201805/20180528/Archer%20C50(EU)_V4_180313.zip>`__ funktioniert.
Das Archiv entpacken und die Firmware (*.bin*) als *tpl.bin* abspeichern.

Dann müssen die beiden zusammengeführt werden, dazu folgende Befehle ausführen:

.. code-block:: sh

   dd if=/dev/zero of=tp_recovery.bin bs=196608 count=1
   dd if=tpl.bin of=tmp.bin bs=131584 count=1
   dd if=tmp.bin of=boot.bin bs=512 skip=1
   cat boot.bin >> tp_recovery.bin
   cat ff.bin >> tp_recovery.bin

Via TFTP flashen
----------------

Der eigene Rechner muss unter 192.168.0.66 erreichbar sein. Es empfiehlt sich, den eigenen Rechner über einen Switch mit dem
Knoten zu verbinden, um auch beim Neustart des Knotens eine permanente Verbindung zu haben.
Der TFTP Server muss die *tp_recovery.bin* ausliefern.

Den Knoten nun ausschalten, mit einem Werkzeug den Reset-Taster halten und wieder einschalten und mindestens 10 Sekunden den
Reset-Taster gedrückt halten.
Der Knoten sollte dann die vorbereitete Firmware flashen und als Freifunkknoten im Config-Mode wieder starten.

