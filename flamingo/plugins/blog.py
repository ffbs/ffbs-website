from flamingo.core.data_model import Content

import logging
logger = logging.getLogger("project.plugin.blog")

class Blog:

    def contents_parsed(self, context):

        #collect 'blog'-update snippets from all sites
        for content in context.contents:
            if "blog" in content:
                for b in content["blog"]:
                    if not ("date" in b and "title" in b and "body" in b):
                        logger.warn("blog entry in article {} is malformed! 'date', 'title' or 'body' is missing!".format(content["path"]))
                        continue
                    c = Content()
                    c["date"] = b["date"]
                    c["content_title"] = b["title"]
                    c["content_body"] = b["body"]
                    c["url"] = "/"+content["output"]
                    c["path"] = "/dev/null"
                    c["output"] = "/dev/null"
                    c["feedtag"] = True
                    context.contents.add(c)

        # collect legacy firmware releases
        for content in context.contents.filter(path__startswith="firmware/v").order_by("-content_title"):
            for branch in content["branches"]:
                if not ("date" in branch and "channel" in branch and "name" in branch):
                    logger.warn("firmware entry in file {} is malformed! 'date', 'channel' or 'name' missing for branch {}".format(content["path"], branch))
                    continue
                c = Content()
                c["date"] = branch["date"]
                c["content_title"] = "Neue Firmware {} freigegeben".format(content["title"])
                c["content_body"] = "Es wurde eine neue Firmware im Zweig '{}' freigegeben. Das Release heißt '{}'.".format(branch["channel"], branch["name"])
                c["url"] = "/mitmachen/firmware.html"
                c["path"] = "/dev/null"
                c["output"] = "/dev/null"
                c["feedtag"] = True
                context.contents.add(c)

        # collect parker firmware releases
        for content in context.contents.filter(path__startswith="parker/firmware/v").order_by("-content_title"):
            for branch in content["branches"]:
                if not ("date" in branch and "channel" in branch and "name" in branch):
                    logger.warn("firmware entry in file {} is malformed! 'date', 'channel' or 'name' missing for branch {}".format(content["path"], branch))
                    continue
                c = Content()
                c["date"] = branch["date"]
                c["content_title"] = "Neue Firmware {} freigegeben".format(content["title"])
                c["content_body"] = "Es wurde eine neue Firmware im Zweig '{}' freigegeben. Das Release heißt '{}'.".format(branch["channel"], branch["name"])
                c["url"] = "/parker/firmware.html"
                c["path"] = "/dev/null"
                c["output"] = "/dev/null"
                c["feedtag"] = True
                context.contents.add(c)
