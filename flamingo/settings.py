import os

PLUGINS = [
    'flamingo.plugins.Redirects',
    'flamingo.plugins.rstBootstrap3',
    'flamingo.plugins.rstPygments',
    'plugins/title.py::Title',
    'plugins/git.py::Git',
    'plugins/raw.py::Raw',
    'plugins/rst_ffbs_box.py::rstFfbsBox',
    'plugins/blog.py::Blog',
    'flamingo.plugins.Feeds',
]

# remove flamingo core HTML plugin.
# we can now use our own "Raw"
DEFAULT_PLUGINS = [x for x in DEFAULT_PLUGINS if  x != "flamingo.plugins.HTML"]

POST_BUILD_LAYERS = [
        'static',
]

THEME_PATHS = [
        'theme/',
]

FEEDS_DOMAIN = 'https://freifunk-bs.de'
FEEDS = [
    {
        'id': 'freifunk-bs.de/feed',
        'title': 'freifunk-bs.de',
        'type': 'atom',
        'output': 'all.atom.xml',
        'lang': 'en',
        'contents':
            lambda ctx: ctx.contents.filter(feedtag=True).order_by("date"),
        'entry-id':
            lambda content: 'tag:freifunk-bs.de,{}:/{}'.format(
                content['date'],
                os.path.basename(content['url']),
            ),
        'published':
            lambda content: content['date'].strftime('%Y-%m-%d %H:%M:%S+01:00'),
        'updated':
            lambda content: content['date'].strftime('%Y-%m-%d %H:%M:%S+01:00'),
    },
]
